import hashlib
from sqlite3 import Connection

from entity.User import User


class UserService:
    def __init__(self, connection: Connection):
        self.connection = connection

    def hash_password(self, password: str):
        return hashlib.sha512(password.encode('utf-8')).hexdigest()

    def create_user(self, username: str, password: str):
        c = self.connection.cursor()
        c.execute("INSERT INTO users (username, password) VALUES (?, ?)", [
            username,
            self.hash_password(password)
        ])
        c.execute("SELECT LAST_INSERT_ROWID()")
        user_id = c.fetchone()[0]
        self.connection.commit()
        return User(user_id, username)

    def authenticate(self, username: str, password: str):
        c = self.connection.cursor()
        c.execute("SELECT user_id, password FROM users WHERE users.username = ?", [username])
        result = c.fetchone()
        if result is not None:
            user_id = result[0]
            password_hash = result[1]
            if self.hash_password(password) == password_hash:
                return User(user_id, username)
            else:
                return None
        else:
            return None
