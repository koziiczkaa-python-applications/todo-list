import sqlite3
from pathlib import Path

from entity.User import User
from service.TaskService import TaskService
from service.UserService import UserService


class EntityManager:
    def __init__(self, database_file: str):
        database_path = Path()
        database_path.parent.mkdir(exist_ok=True)
        self.connection = sqlite3.connect(database_file)

        c = self.connection.cursor()
        c.execute("PRAGMA foreign_keys = ON")  # dev only
        c.execute("""
            CREATE TABLE IF NOT EXISTS users (
                user_id integer PRIMARY KEY AUTOINCREMENT, 
                username text NOT NULL UNIQUE, 
                password text NOT NULL
            )
        """)
        c.execute("""
            CREATE TABLE IF NOT EXISTS tasks (
                task_id integer PRIMARY KEY AUTOINCREMENT,
                user_id integer NOT NULL,
                name text NOT NULL,
                description text NOT NULL,
                due_date text NOT NULL,
                FOREIGN KEY (user_id) REFERENCES users (user_id)
            )
        """)
        self.connection.commit()

    def create_user_service(self):
        return UserService(self.connection)

    def create_task_service(self, user: User):
        return TaskService(self.connection, user)
