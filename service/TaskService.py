import datetime
from sqlite3 import Connection

from entity.Task import Task
from entity.User import User


class TaskService:
    def __init__(self, connection: Connection, user: User):
        self.connection = connection
        self.user = user

    def create_task(self, task: Task):
        c = self.connection.cursor()
        c.execute("INSERT INTO tasks (name, user_id, description, due_date) VALUES (?, ?, ?, ?)", [
            task.name,
            self.user.id,
            task.description,
            str(task.due_date.strftime("%d.%m.%Y %H:%M"))
        ])
        self.connection.commit()

    def edit_task(self, task: Task):
        c = self.connection.cursor()
        c.execute("UPDATE tasks SET name = ?, description = ?, due_date = ? WHERE task_id = ?", [
            task.name,
            task.description,
            str(task.due_date.strftime("%d.%m.%Y %H:%M")),
            task.id
        ])
        self.connection.commit()

    def delete_task(self, task_id: int):
        c = self.connection.cursor()
        c.execute("DELETE FROM tasks WHERE task_id = ? AND user_id = ?", [task_id, self.user.id])  # dodac where user_id
        self.connection.commit()

    def task_exists(self, task_id: int):
        c = self.connection.cursor()
        c.execute("SELECT 1 FROM tasks WHERE task_id = ? AND user_id = ?", [task_id, self.user.id])
        result = c.fetchone()
        return result is not None

    def get_tasks(self):
        tasks = []

        c = self.connection.cursor()
        c.execute("SELECT task_id, user_id, name, description, due_date FROM tasks WHERE user_id = ?", [self.user.id])
        for row in c.fetchall():
            task_id = row[0]
            user_id = row[1]
            name = row[2]
            description = row[3]
            due_date = datetime.datetime.strptime(row[4], "%d.%m.%Y %H:%M")
            task = Task(task_id, user_id, name, description, due_date)
            tasks.append(task)
        return tasks
