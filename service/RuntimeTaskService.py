from entity.Task import Task


class RuntimeTaskService:
    next_id = 0
    tasks_list = []

    def get_task(self, task_id):
        value = filter(lambda x: x.id == task_id, self.tasks_list)
        result = list(value)
        if len(result) == 0:
            return None
        return result[0]

    def create_task(self, task_name, task_description, task_due_date):
        task_id = self.next_id
        self.next_id += 1
        self.tasks_list.append(Task(task_id, task_name, task_description, task_due_date))

    def delete_task(self, task):
        if isinstance(task, Task):
            if not self.task_exists(task):
                raise RuntimeError("This task already doesn't exist")
            self.tasks_list.remove(task)
        elif isinstance(task, int):
            target = self.get_task(task)
            if target is None:
                raise RuntimeError("Task with this id already doesn't exist")
            self.tasks_list.remove(target)
        else:
            raise RuntimeError("Argument must be either of type Type or int")

    def edit_task(self, task):
        found = self.get_task(task.id)

        if found is None:
            raise RuntimeError("Task with given id doesn't exist")

        index = self.tasks_list.index(found)
        self.tasks_list.remove(found)
        self.tasks_list.insert(index, task)

    def task_exists(self, task):
        found = self.get_task(task)
        return found is not None

    def __str__(self):
        return '[' + ', '.join(map(str, self.tasks_list)) + ']'

    def __repr__(self):
        return '[' + ', '.join(map(str, self.tasks_list)) + ']'
