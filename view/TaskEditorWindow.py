import datetime

from PyQt5.QtCore import pyqtSignal
from PyQt5.QtWidgets import QDialog
from PyQt5.uic import loadUi

from entity.Task import Task


class TaskEditorWindow(QDialog):
    task = None
    user = None
    saved = pyqtSignal(Task)

    def __init__(self, user=None, task=None):
        super(TaskEditorWindow, self).__init__()
        self.task = task
        self.user = user

        loadUi("resources/todolist-edit.ui", self)
        self.setFixedSize(925, 400)
        self.pushButton.clicked.connect(self.handle_save_button)

        if isinstance(task, Task):
            title = 'Edytuj zadanie ' + str(task.id)
            self.load_task(task)
        else:
            title = 'Stwórz nowe zadanie'
            self.dateTimeEdit.setDateTime(datetime.datetime.now())
            
        self.setWindowTitle(title)
        self.label.setText(title)

    def load_task(self, task):
        self.textEdit.setPlainText(task.name)
        self.textEdit_2.setPlainText(task.description)
        self.dateTimeEdit.setDateTime(task.due_date)  # date -> Task

    def handle_save_button(self):
        task_name = self.textEdit.toPlainText()
        task_description = self.textEdit_2.toPlainText()
        task_due_date = self.dateTimeEdit.dateTime().toPyDateTime()

        if self.task is not None:
            self.task.name = task_name
            self.task.description = task_description
            self.task.due_date = task_due_date

            self.saved.emit(self.task)
        else:
            new_task = Task(None, self.user, task_name, task_description, task_due_date)
            self.saved.emit(new_task)
