from PyQt5 import QtWidgets
from PyQt5.QtCore import pyqtSignal
from PyQt5.QtWidgets import QDialog
from PyQt5.uic import loadUi

from entity.User import User
from service.UserService import UserService


class SignupWindow(QDialog):
    signed_up = pyqtSignal(User)

    def __init__(self, user_service: UserService):
        super(SignupWindow, self).__init__()

        self.setFixedSize(500, 400)

        self.user_service = user_service

        loadUi("resources/todolist-signup-form.ui", self)
        self.signupButton.clicked.connect(self.handle_signup_button)
        self.label_5.setHidden(True)
        self.lineEdit_2.setEchoMode(QtWidgets.QLineEdit.Password)
        self.lineEdit_3.setEchoMode(QtWidgets.QLineEdit.Password)
        self.setWindowTitle('Rejestracja')

    def handle_signup_button(self):
        username = self.lineEdit.text()
        password = self.lineEdit_2.text()
        repeated_password = self.lineEdit_3.text()

        if password:
            if password != repeated_password:
                self.label_5.setText('Hasła sie różnią')
                self.label_5.setHidden(False)
            else:
                new_user = self.user_service.create_user(username, password)
                self.signed_up.emit(new_user)
        else:
            self.label_5.setText('Wypełnij wszystkie pola')
            self.label_5.setHidden(False)
