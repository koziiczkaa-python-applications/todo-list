from PyQt5 import QtWidgets
from PyQt5.QtCore import pyqtSignal
from PyQt5.QtWidgets import QDialog
from PyQt5.uic import loadUi

from entity.User import User
from service.UserService import UserService


class LoginWindow(QDialog):
    logged_in = pyqtSignal(User)

    def __init__(self, user_service: UserService):
        super(LoginWindow, self).__init__()

        self.setFixedSize(500, 400)

        self.user_service = user_service

        loadUi("resources/todolist-login-form.ui", self)
        self.pushButton.clicked.connect(self.handle_login_button)
        self.lineEdit_2.setEchoMode(QtWidgets.QLineEdit.Password)
        self.label_4.setHidden(True)

        self.setWindowTitle('Logowanie')

    def handle_login_button(self):
        username = self.lineEdit.text()
        password = self.lineEdit_2.text()

        user = self.user_service.authenticate(username, password)

        if isinstance(user, User):
            self.logged_in.emit(user)
        else:
            self.label_4.setHidden(False)
