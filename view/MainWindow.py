from PyQt5 import QtWidgets
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QDialog, QPushButton, QHBoxLayout, QWidget, QVBoxLayout
from PyQt5.uic import loadUi

from entity.Task import Task
from entity.User import User
from service.EntityManager import EntityManager
from view.LoginWindow import LoginWindow
from view.SignupWindow import SignupWindow
from view.TaskEditorWindow import TaskEditorWindow


class MainWindow(QDialog):
    current_user = None

    def __init__(self, entity_manager: EntityManager):
        super(MainWindow, self).__init__()
        self.user = None
        self.task_service = None
        self.entity_manager = entity_manager
        self.user_service = entity_manager.create_user_service()

        loadUi("resources/todolist.ui", self)
        self.tableWidget.verticalHeader().hide()
        self.tableWidget.setColumnWidth(0, 50)
        self.tableWidget.setColumnWidth(1, 300)
        self.tableWidget.setColumnWidth(2, 550)
        self.tableWidget.setColumnWidth(3, 205)
        self.tableWidget.setColumnWidth(4, 250)

        self.pushButton.clicked.connect(self.handle_login_button)
        self.pushButton_3.clicked.connect(self.handle_signup_button)

        log_out_button = QPushButton(self.frame)

        buttons_box = QVBoxLayout(self.frame)
        buttons_box.addWidget(log_out_button, 1, alignment=Qt.AlignRight)
        self.log_out_button = log_out_button
        self.log_out_button.setText('Wyloguj')
        self.log_out_button.setToolTip('Wyloguj się')
        self.log_out_button.setFixedSize(101, 31)
        self.log_out_button.hide()
        self.pushButton_2.hide()

        self.log_out_button.clicked.connect(self.handle_logout)

        self.pushButton_2.clicked.connect(self.handle_create_task_button)

    def load_data(self):
        row = 0
        tasks = self.task_service.get_tasks()

        self.tableWidget.setRowCount(len(tasks))
        for single_task in tasks:
            widget = QWidget()
            layout = QHBoxLayout()

            delete_btn = QPushButton(self.tableWidget)
            delete_btn.setIcon(QIcon('resources/trash.svg'))
            delete_btn.setText('Usuń')
            delete_btn.setToolTip('Usuń zadanie')
            delete_btn.clicked.connect(lambda checked, task=single_task: self.delete_task(task.id))
            layout.addWidget(delete_btn)

            edit_btn = QPushButton(self.tableWidget)
            edit_btn.setIcon(QIcon('resources/edit.svg'))
            edit_btn.setText('Edytuj')
            edit_btn.setToolTip('Edytuj zadanie')
            edit_btn.clicked.connect(lambda checked, task=single_task: self.handle_edit_button(task))
            layout.addWidget(edit_btn)
            layout.setContentsMargins(0, 0, 0, 0)
            layout.setSpacing(0)
            widget.setLayout(layout)

            self.tableWidget.setItem(row, 0, QtWidgets.QTableWidgetItem(str(single_task.id)))
            self.tableWidget.setItem(row, 1, QtWidgets.QTableWidgetItem(single_task.name))
            self.tableWidget.setItem(row, 2, QtWidgets.QTableWidgetItem(single_task.description))
            self.tableWidget.setItem(row, 3, QtWidgets.QTableWidgetItem(str(single_task.due_date)))
            self.tableWidget.setCellWidget(row, 4, widget)

            row += 1

    def handle_edit_button(self, task: Task):
        d = TaskEditorWindow(self.user, task)
        d.saved.connect(lambda t: (
            d.close(),
            self.edit_task(t)
        ))
        d.exec_()

    def handle_logout(self):
        self.current_user = None
        self.tableWidget.setRowCount(0)
        self.pushButton.show()
        self.pushButton_3.show()
        self.log_out_button.hide()
        self.pushButton_2.hide()

    def handle_create_task_button(self):
        d = TaskEditorWindow(self.user)
        d.saved.connect(lambda nt: (
            d.close(),
            self.add_task(nt)
        ))
        d.exec_()

    def handle_login_button(self):
        d = LoginWindow(self.user_service)
        d.logged_in.connect(lambda u: (
            d.close(),
            self.handle_log_in(u),
        ))
        d.exec_()

    def handle_log_in(self, user: User):
        self.task_service = self.entity_manager.create_task_service(user)
        self.user = user
        self.pushButton.hide()
        self.pushButton_3.hide()
        self.pushButton_2.show()
        self.log_out_button.show()
        self.current_user = user
        self.load_data()

    def handle_signup_button(self):
        d = SignupWindow(self.user_service)
        d.signed_up.connect(lambda u: (
            d.close(),
            self.handle_sign_up(u)
        ))
        d.exec_()

    def handle_sign_up(self, user: User):
        self.task_service = self.entity_manager.create_task_service(user)
        self.user = user

    def handle_save_task(self):
        return

    def delete_task(self, task_id):
        self.task_service.delete_task(task_id)
        self.load_data()

    def edit_task(self, task: Task):
        self.task_service.edit_task(task)
        self.load_data()

    def add_task(self, task: Task):
        self.task_service.create_task(task)
        self.load_data()
