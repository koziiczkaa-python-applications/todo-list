import sys

from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QApplication

from view.MainWindow import MainWindow


class ApplicationGui(QApplication):
    def __init__(self, entity_manager):
        super(ApplicationGui, self).__init__(sys.argv)
        main_window = MainWindow(entity_manager)
        widget = QtWidgets.QStackedWidget()
        widget.addWidget(main_window)
        widget.setFixedWidth(main_window.width())
        widget.setFixedHeight(main_window.height())
        widget.show()
        try:
            sys.exit(self.exec_())
        except:
            print('Exit')
