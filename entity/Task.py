import datetime
from typing import Union

from entity.User import User


class Task:
    def __init__(self, id: Union[int, None], user: User, name: str, description: str, due_date: datetime):
        self.id = id
        self.user = user
        self.name = name
        self.description = description
        self.due_date = due_date

    def __eq__(self, other):
        if not isinstance(other, Task):
            return NotImplemented
        return self.id == other.id and \
               self.user == other.user and \
               self.name == other.user and \
               self.description == other.description and \
               self.due_date == other.due_date

    def __str__(self):
        return 'Task(id=' + str(
            self.id) + ', name=' + self.name + ', description=' + self.description + ', due_date=' + str(
            self.due_date) + ')'

    def __repr__(self):
        return 'Task(id=' + str(
            self.id) + ', name=' + self.name + ', description=' + self.description + ', due_date=' + str(
            self.due_date) + ')'
