from typing import Union


class User:
    def __init__(self, id: Union[int, None], username: str):
        self.id = id
        self.username = username

    def __eq__(self, other):
        if not isinstance(other, User):
            return NotImplemented
        return self.id == other.id and \
               self.username == other.username
