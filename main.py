import datetime
import sys

from entity.Task import Task
from service.EntityManager import EntityManager
from view.ApplicationGui import ApplicationGui

entity_manager = EntityManager("data/database.db")

if '-nogui' in sys.argv:
    user_service = entity_manager.create_user_service()
    current_user = None

    while True:
        if current_user is None:
            print('What do you want to do?')
            print('0. Exit')
            print('1. Log in')
            print('2. Sign up')

            choice = int(input('Enter your choice: '))

            if choice == 0:
                break
            elif choice == 1:
                login = input('Provide your login: ')
                password = input('Provide your password: ')

                user = user_service.authenticate(login, password)

                if user is None:
                    print('Wrong log in data')
                else:
                    print('Logged in succesfully!')
                    current_user = user
            elif choice == 2:
                login = input('Provide login: ')
                password = input('Provide password: ')
                repeated_password = input('Provide password again: ')

                if password != repeated_password:
                    print('Passwords are different')
                else:
                    print('Signed up successfully!')
                    user_service.create_user(login, password)
            else:
                print('Wrong choice!')
        else:
            print('What do you want to do?')
            print('0. Exit')
            print('1. Show tasks')
            print('2. Create task')
            print('3. Edit task')
            print('4. Delete task')
            print('5. Log out')

            task_service = entity_manager.create_task_service(current_user)

            choice = int(input('Enter your choice: '))

            if choice == 0:
                break
            elif choice == 1:
                for task in task_service.get_tasks():
                    print(task)
            elif choice == 2:
                task_name = input("Insert name: ")
                task_description = input("Insert description: ")
                task_due_date = input("Insert due date: ")

                try:
                    datetime.datetime.strptime(task_due_date, '%d.%m.%Y %H:%M')
                except ValueError:
                    print("Incorrect date string format. The format should be DD.MM.YYYY hh:mm")

                task_service.create_task(
                    Task(
                        None,
                        current_user,
                        task_name,
                        task_description,
                        datetime.datetime.strptime(task_due_date, '%d.%m.%Y %H:%M')
                    )
                )
            elif choice == 3:
                task_id = int(input("Provide id of the task you want to edit: "))

                if task_service.task_exists(task_id):
                    task_name = input("Insert new name: ")
                    task_description = input("Insert new description: ")
                    task_due_date = input("Insert new due date: ")

                    try:
                        datetime.datetime.strptime(task_due_date, '%d.%m.%Y %H:%M')
                    except ValueError:
                        print("Incorrect date string format. The format should be DD.MM.YYYY hh:mm")

                    task_service.edit_task(
                        Task(
                            task_id,
                            current_user,
                            task_name,
                            task_description,
                            datetime.datetime.strptime(task_due_date, '%d.%m.%Y %H:%M')
                        )
                    )
                    print("Task has been edited successfully")
                else:
                    print("Task with given id doesn't exist")
            elif choice == 4:
                task_id = int(input("Provide id of the task you want to remove: "))

                if task_service.task_exists(task_id):
                    task_service.delete_task(task_id)
                    print("Task has been removed successfully")
                else:
                    print("Task with given id doesn't exist")
            elif choice == 5:
                current_user = None
                print('You logged out successfully!')
            else:
                print('Wrong choice!')
else:
    app = ApplicationGui(entity_manager)
